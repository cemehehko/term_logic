#include <ce/logger.h>
#include <ce/container.h>

#include "termlogic.h"

TermLogic::TermLogic( ) { }

int TermLogic::init(const std::string &s, const ce_container_ctx_t &ctx)
{
    _flattener = new flat::Flattener(_log->name());
    if (auto res = parseSettings(s))
        return res;
    auto link = _links.find("input");
    if (link != _links.end() && link->second.size() == 1) {
        _inLink = link->second[0];
    } else {
        return _log->error_ret(1, "Incorrect input link");
    }

    link = _links.find("flush");
    if (link != _links.end()) {
        if (link->second.size() == 1)
            _flushLink = link->second[0];
        else
            return _log->error_ret(1, "Incorrect count of flush links, val: %lu, expected: 1|0", link->second.size());
    } else {
        _flushLink = nullptr;
    }

    if (auto drop = _cfg.getT("drop-before", false))
        _dropBefore = *drop;
    else
        return _log->error_ret(EINVAL, "drop-before %s, expected: true|false|yes|no|1|0", drop.c_str());

    if (auto autoTable = _cfg.getT("auto-table", true))
        _autoTable = *autoTable;
    else
        return _log->error_ret(EINVAL, "auto-table %s, expected: true|false|yes|no|1|0", autoTable.c_str());

    if (auto unlogged = _cfg.getT("unlogged-table", true))
        _unlogged = *unlogged;
    else
        return _log->error_ret(EINVAL, "unlogged-table: %s, expected: true|false|yes|no|1|0", unlogged.c_str());

    if (auto seqDuplicate = _cfg.getT("seq-duplicate", false)) {
        _seqDuplicate = *seqDuplicate;
    } else {
        return _log->error_ret(EINVAL,
                "seq-duplicate: %s, expected: true|false|yes|no|1|0",
                seqDuplicate.c_str());
    }

    auto contConf = ce::Config(const_cast<ce_config_t *>(ce_container_config(_container)));
    const auto seqKey = ce::strprintf("logic.%s.seq",_name.c_str());
    auto confSeq = contConf.get(seqKey);
    if (confSeq)
        _confString = *confSeq;

    contConf.set_cb(seqKey, configCallback, this);

    _schema = ce_schema_new(_cfg.get("schema", "").c_str());
    if (auto res = processSchemaOptions())
        return res;
    return 0;
}

int TermLogic::open(const std::string &s)
{
    _pConnDb = PQconnectdb(_connConfig.c_str());
    if (PQstatus(_pConnDb) != CONNECTION_OK)
        return _log->error_ret(ECONNREFUSED, "Cannot connect to DB, settings: %s", _connConfig.c_str());

    if (auto res = processOldTables())
        return res;
    if (auto res = processNewTables())
        return res;
    return 0;
}

void TermLogic::close( )
{
    for (auto &kv : _msgProc) {
        auto &msgProc = kv.second;
        msgProc.msgBatchSize = 0;
        msgProc.msgBuffers.clear();
        msgProc.uniqMsg.clear();
        for (auto &el : msgProc.msgColumns)
            el.dbDataOffset.clear();
    }
    _lastSeq = _currentSeq = -1;
    if (_pConnDb) {
        PQfinish(_pConnDb);
        _pConnDb = nullptr;
    }
}

int TermLogic::process(const ce::Link *link, const ce_msg_t *msg)
{
    int res = 0;
    switch (msg->type) {
    case CE_MSG_TYPE_DATA: {
        res = processDataMsg(link, msg);
        break;
    }
    case CE_MSG_TYPE_STATE: {
        res = processStateMsg(link, msg);
        break;
    }
    case CE_MSG_TYPE_SYSTEM: {
        res = processSysMsg(link, msg);
        break;
    }
    }
    return res;
}

int TermLogic::pushDataToDb(MsgProc &msgProc)
{
    if (msgProc.msgBatchSize) {
        auto &logMsgBuf = msgProc.msgBuffers;

        const auto upsertMode = msgProc.msgUpsertColList.size();
        const auto msgName = msgProc.table + (upsertMode ? "_temp" : "");
        _log->trace("Push data to DB (%s), size = %d", msgProc.table.c_str(), msgProc.msgBatchSize);
        //write end of transaction
        writeToBuf(&logMsgBuf, END_OF_BATCH_BYTES, END_OF_BATCH_SIZE);

        auto res = PQexecPrepared(_pConnDb,
                ce::strprintf("%s_prep_copy", msgProc.table.c_str()).c_str(),
                0,
                nullptr,
                nullptr,
                nullptr,
                0);
        auto status = PQresultStatus(res);
        PQclear(res);
        if (status != PGRES_COPY_IN) {
            return _log->error_ret(1, "Can't access to copy mode, what: %s",
                    PQerrorMessage(_pConnDb));
        }
       _log->trace("Data transfer started");

        if (upsertMode) { //UpsertMode
            int copyRes = PQputCopyData(_pConnDb, HEADER_BYTES, HEADER_SIZE);
            if (copyRes == 1) {
                for (const auto &kv : msgProc.uniqMsg) {
                    const auto &msgBuf = kv.second;
                    copyRes = PQputCopyData(_pConnDb, (char*) msgBuf.data(), msgBuf.size());
                    if (copyRes == 0)
                        return _log->error_ret(1, "Send no data, connection is in nonblocking mode");
                    else if (copyRes == -1)
                        return _log->error_ret(1, "copyRes == -1: %s", PQerrorMessage(_pConnDb));
                }

                copyRes = PQputCopyData(_pConnDb, END_OF_BATCH_BYTES, END_OF_BATCH_SIZE);
                if (copyRes == 0)
                    return _log->error_ret(1, "Send no data, connection is in nonblocking mode");
                else if (copyRes == -1)
                    return _log->error_ret(1, "copyRes == -1: %s", PQerrorMessage(_pConnDb));

                if (PQputCopyEnd(_pConnDb, NULL) == 1) {
                    res = PQgetResult(_pConnDb);
                    auto status = PQresultStatus(res);
                    PQclear(res);
                    if (status != PGRES_COMMAND_OK)
                        return _log->error_ret(1, "Failed to end copy, what: %s", PQerrorMessage(_pConnDb));
                }
            } else if (copyRes == 0)
                return _log->error_ret(1, "Send no data, connection is in nonblocking mode");
            else if (copyRes == -1)
                return _log->error_ret(1, "copyRes == -1: %s", PQerrorMessage(_pConnDb));
        } else {    //Log mode
            int copyRes = PQputCopyData(_pConnDb, (char*) logMsgBuf.data(), logMsgBuf.size());
            if (copyRes == 1) {
                if (PQputCopyEnd(_pConnDb, NULL) == 1) {
                    res = PQgetResult(_pConnDb);
                    auto status = PQresultStatus(res);
                    PQclear(res);
                    if (status == PGRES_COMMAND_OK) {
                        logMsgBuf.clear();
                        writeToBuf(&logMsgBuf, HEADER_BYTES, HEADER_SIZE);
                    } else {
                        return _log->error_ret(1, "%s", PQerrorMessage(_pConnDb));
                    }
                } else {
                    return _log->error_ret(1, "%s", PQerrorMessage(_pConnDb));
                }
            } else if (copyRes == 0) {
                return _log->error_ret(1, "Send no data, connection is in nonblocking mode");
            } else if (copyRes == -1) {
                return _log->error_ret(1, "copyRes == -1: %s", PQerrorMessage(_pConnDb));
            }
        }
       _log->trace("Data transfer finished");
        if (upsertMode) { //Upsert from temp table into permanent
            res = PQexecPrepared(_pConnDb,
                    ce::strprintf("%s_prep_upsert", msgProc.table.c_str()).c_str(), 0, nullptr,
                    nullptr, nullptr, 0);

            auto status = PQresultStatus(res);
            PQclear(res);
            if (status != PGRES_COMMAND_OK) {
                return _log->error_ret(1, "Can't execute upsert prepared statement, what: %s",
                        PQerrorMessage(_pConnDb));
            }
            _log->trace("Data moved from temporary table");
            //drop temp table async
            PQsendQuery(_pConnDb, ce::strprintf("TRUNCATE %s;", msgName.c_str()).c_str());
            _log->trace("Temporary table truncated");
        }
        msgProc.msgBatchSize = 0;
    } else {
        _log->trace("Nothing to push for %s", msgProc.table.c_str());
    }
    return 0;
}

int TermLogic::parseSettings(const std::string &params)
{
    ce::Url url(params);

    auto host = url.get("host");
    if (host)
        _connConfig += " host=" + *host;

    auto port = url.get("port");
    if (port)
        _connConfig += " port=" + *port;

    auto dbname = url.get("dbname");
    if (port)
        _connConfig += " dbname=" + *dbname;

    auto user = url.get("user");
    if (user)
        _connConfig += " user=" + *user;

    auto password = url.get("password");
    if (user)
        _connConfig += " password=" + *password;

    auto timeout = url.getT<ce::time::seconds>("connect_timeout", ce::time::seconds(2));
    _connConfig += " connect_timeout=" + std::to_string(timeout->count());

    auto maxBatchSize = url.getT<uint32_t>("batch_size", 10000);
    if (maxBatchSize)
        _maxBatchSize = *maxBatchSize;
    else
        return _log->error_ret(EINVAL, "Incorrect batch_size value.");

    return 0;
}

int TermLogic::writeMessage(const ce_msg_t *msg, MsgProc &msgProc)
{
    auto newMsgBuf = &msgProc.msgBuffers;  // Log mode
    _log->trace("writeMessage seq=%zd", msg->seq);
    auto inMsgData = (char*) msg->data;
    auto &columns = msgProc.msgColumns;

    //Upsert mode
    const bool upsertMode = msgProc.msgUpsertColList.size();
    const bool &pmapMode = msgProc.msgUpsertPmap;

    std::string key;
    //Предполагаем, что первые 8 байт сообщения - pmap
    uint64_t pmapVal = *(uint64_t*) inMsgData;

    for (const auto &keyCol : msgProc.msgUpsertColList) {
        if (upsertMode) { //Key
            auto col = columns.begin();
            for (; col != columns.end(); ++col) {
                if (col->pField->name == keyCol)
                    break;
            }
            if (col == columns.end())
                return _log->error_ret(1, "Can't found key column %s for message %s", keyCol.c_str(), msg->name);

            if (col->pField->type == ce_schema_field_t::VString) {
                auto optr = (const ce::offset_ptr_t<char>*) (inMsgData + col->pField->offset);
                if (!optr->size())
                    return _log->error_ret(EINVAL, "Incorrect VString key");
                key += std::string(optr->ptr(), optr->size() - 1);
            } else if (col->pField->type == ce_schema_field_t::Decimal128 ||
                    col->pField->type == ce_schema_field_t::Message)
            {
                return _log->error_ret(EINVAL, "Incorrect key type %d", col->pField->type);
            } else {
                key += std::string(inMsgData + col->pField->offset, col->pField->size);
            }

            if (pmapMode) {
                newMsgBuf = new ce::buffer_t;
            } else {
                newMsgBuf = &msgProc.uniqMsg[key];
                newMsgBuf->clear();
            }

            //2 - int16 columns cnt, 4 - int32 size of column
            newMsgBuf->reserve(msg->size + COL_CNT_SIZE + COL_SIZE * (columns.size() + 1));
        }
    }

    //write columns count
    int16_t colCnt = htons((int16_t ) (columns.size() + 1));
    writeToBuf(newMsgBuf, &colCnt, sizeof(int16_t));

    for (auto &col : columns) {
        //Last nullable value
        if (upsertMode && pmapMode && !ce_schema_pmap_check(pmapVal, col.pField->index)) {
            auto oldMsgIt = msgProc.uniqMsg.find(key);
            auto oldOffs = col.dbDataOffset.find(key);

            if (oldMsgIt != msgProc.uniqMsg.end() && oldOffs != col.dbDataOffset.end()) {
                _log->trace("Old message found");
                char *oldField = (char*) oldMsgIt->second.begin() + oldOffs->second;
                int32_t oldFieldSize = ntohl(*(int32_t* ) oldField);
                if (oldFieldSize < 0) //Old field is NULL
                    oldFieldSize = 0;

                writeToBuf(newMsgBuf, oldField, oldFieldSize + sizeof(int32_t));
                oldOffs->second = (size_t) newMsgBuf->end() - (size_t) newMsgBuf->begin()
                                  - oldFieldSize - sizeof(int32_t);
            } else {
                _log->trace("New message");
                newMsgBuf->reserve(newMsgBuf->size() + sizeof(int32_t));
                *(int32_t*) newMsgBuf->end() = PG_NULL;
                col.dbDataOffset[key] = (size_t) newMsgBuf->end() - (size_t) newMsgBuf->begin();
                newMsgBuf->resize(newMsgBuf->size() + sizeof(int32_t));
            }
        } else {
            col.dbDataOffset[key] = (size_t) newMsgBuf->end() - (size_t) newMsgBuf->begin();
            _log->trace("Log message");

            //write column value
            switch (col.pField->type) {
            case ce_schema_field_t::VString: {
                auto optr = (const ce::offset_ptr_t<char>*) (inMsgData + col.pField->offset);
                if (!optr->size())
                    return _log->error_ret(EINVAL, "Incorrect VString");
                int32_t fieldSize = optr->size() - 1;
                if (((optr->ptr()[fieldSize - 1] & 0xe0) ^ 0xc0) == 0) {
                    fieldSize = htonl(fieldSize - 1);
                    _log->warn("Incompatible sequence of utf8 character in the end of string, field: %s, seq: %ld, msg_name: %s",
                            col.pField->name, msg->seq, msg->name);
                } else {
                    fieldSize = htonl(fieldSize);
                }
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                writeToBuf(newMsgBuf, optr->ptr(), fieldSize);
                break;
            }
            case ce_schema_field_t::String: {
                int32_t fieldSize = strlen(inMsgData + col.pField->offset);
                if ((((inMsgData + col.pField->offset)[fieldSize - 1] & 0xe0) ^ 0xc0) == 0) {
                    fieldSize = htonl(fieldSize - 1);
                    _log->warn("Incompatible sequence of utf8 character in the end of string, field: %s, seq: %ld, msg_name: %s",
                            col.pField->name, msg->seq, msg->name);
                } else {
                    fieldSize = htonl(fieldSize);
                }
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                writeToBuf(newMsgBuf, inMsgData + col.pField->offset, fieldSize);
                break;
            }
            case ce_schema_field_t::Bytes: {
                int32_t fieldSize = htonl((int32_t) col.pField->size);
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                writeToBuf(newMsgBuf, inMsgData + col.pField->offset, fieldSize);
                break;
            }
            case ce_schema_field_t::Int8: {
                int32_t fieldSize = htonl((int32_t)sizeof(int16_t));
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                uint16_t fieldData = htons(*(uint8_t *) (inMsgData + col.pField->offset));
                writeToBuf(newMsgBuf, &fieldData, fieldSize);
                break;
            }
            case ce_schema_field_t::Int16: {
                int32_t fieldSize = htonl((int32_t) col.pField->size);
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                uint16_t fieldData = htons(*(uint16_t* ) (inMsgData + col.pField->offset));
                writeToBuf(newMsgBuf, &fieldData, fieldSize);
                break;
            }
            case ce_schema_field_t::Enum: {
                if (!col.enumAsString) {
                    int32_t fieldSize = htonl(sizeof(int32_t));
                    writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                    fieldSize = ntohl(fieldSize);
                    switch (col.pField->enum_desc->size) {
                    case 1: {
                        uint32_t fieldData = htonl((uint32_t)*(uint8_t*) (inMsgData + col.pField->offset));
                        writeToBuf(newMsgBuf, &fieldData, sizeof(int32_t));
                        break;
                    }
                    case 2: {
                        uint32_t fieldData = htonl((uint32_t)*(uint16_t*) (inMsgData + col.pField->offset));
                        writeToBuf(newMsgBuf, &fieldData, sizeof(int32_t));
                        break;
                    }
                    case 4: {
                        uint32_t fieldData = htonl(*(uint32_t*) (inMsgData + col.pField->offset));
                        writeToBuf(newMsgBuf, &fieldData, sizeof(int32_t));
                        break;
                    }
                    case 8: {
                        uint64_t fieldData = htonll(*(uint64_t*) (inMsgData + col.pField->offset));
                        writeToBuf(newMsgBuf, &fieldData, sizeof(int64_t));
                        break;
                    }
                    default:
                        return _log->error_ret(EINVAL,
                                "Enums with size not equal to 1,2,4,8 are not compatible, size: %lu, mesageName: %s",
                                col.pField->enum_desc->size, msg->name);
                    }
                } else {
                    int64_t val = 0;
                    switch (col.pField->enum_desc->size) {
                    case 1:
                        val = *(int8_t *)(inMsgData + col.pField->offset);
                        break;
                    case 2:
                        val = *(int16_t *)(inMsgData + col.pField->offset);
                        break;
                    case 4:
                        val = *(int32_t *)(inMsgData + col.pField->offset);
                        break;
                    case 8:
                        val = *(int64_t *)(inMsgData + col.pField->offset);
                        break;
                    default:
                        return _log->error_ret(EINVAL,
                                "Enums with size not equal to 1,2,4,8 are not compatible, size: %lu, mesageName: %s",
                                col.pField->enum_desc->size, msg->name);
                    }
                    auto it = col.pField->enum_desc->values;
                    for (; it; it = it->next) {
                        if (val == it->value) {
                            int32_t fieldSize = htonl((int32_t)strlen(it->name));
                            writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                            fieldSize = ntohl(fieldSize);
                            writeToBuf(newMsgBuf, it->name, fieldSize);
                            break;
                        }
                    }
                    if (!it) {
                        if (upsertMode && pmapMode && newMsgBuf)
                            delete newMsgBuf;
                        _log->error("Incorrect value %ld of Enum %s_%s, skipping, messageName: %s, seq: %ld", val,
                                msgProc.table.c_str(), col.pField->enum_desc->name, msg->name, msg->seq);
                        return 0;
                    }
                }
                break;
            }
            case ce_schema_field_t::Int32: {
                int32_t fieldSize = htonl((int32_t) col.pField->size);
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                uint32_t fieldData = htonl(*(uint32_t* ) (inMsgData + col.pField->offset));
                writeToBuf(newMsgBuf, &fieldData, fieldSize);
                break;
            }
            case ce_schema_field_t::Decimal: {
                auto valBase10 = *(int64_t*) (inMsgData + col.pField->offset);
                makeDecimal(newMsgBuf, valBase10, col.pField->width);
                break;
            }
            case ce_schema_field_t::Decimal128: {
                /*
                 * TODO:
                 * Decimal128 handler
                 */
                return _log->error_ret(EINVAL, "Decimal128 values are not supported");
            }
            case ce_schema_field_t::Int64: {
                int32_t fieldSize = htonl((int32_t) col.pField->size);
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                uint64_t fieldData = *(uint64_t* )(inMsgData + col.pField->offset);
                if (!col.resolution.empty()) {
                    std::string point = std::to_string(fieldData) + col.resolution;
                    auto usTime = ce::duration_cast<ce::time::us>(*ce::conv::to_time(point));
                    fieldData = usTime.count() - U_SEC_1970_2000;
                }
                fieldData = htonll(fieldData);
                writeToBuf(newMsgBuf, &fieldData, fieldSize);
                break;
            }
            case ce_schema_field_t::Double: {
                int32_t fieldSize = htonl((int32_t) col.pField->size);
                writeToBuf(newMsgBuf, &fieldSize, sizeof(int32_t)); //Внести в case
                fieldSize = ntohl(fieldSize);
                uint64_t fieldData = htonll(*(uint64_t* )(inMsgData + col.pField->offset));
                writeToBuf(newMsgBuf, &fieldData, fieldSize);
                break;
            }
            case ce_schema_field_t::Message: {
                if (msgProc.tNumAsNumeric) {
                    auto offsVal = col.pField->offset + col.pField->type_msg->fields->offset;
                    auto offsPrec = col.pField->offset + col.pField->type_msg->fields->next->offset;
                    auto prec = *(int8_t*) ((char*) msg->data + offsPrec);
                    auto valBase10 = *(int64_t*) ((char*) msg->data + offsVal);
                    makeDecimal(newMsgBuf, valBase10, prec);
                    break;
                }
                break;
            }
            default:
                return _log->error_ret(EINVAL, "Incorrect data type: %d", col.pField->type);
            }
        }
    }
    int32_t seqSize = htonl(sizeof(int64_t));
    writeToBuf(newMsgBuf, &seqSize, sizeof(int32_t));
    seqSize = sizeof(int64_t);
    auto seq = htonll(msg->seq);
    writeToBuf(newMsgBuf, &seq, seqSize);
    if (upsertMode && pmapMode) {
        msgProc.uniqMsg[key].clear();
        writeToBuf(&msgProc.uniqMsg[key], newMsgBuf->begin(), newMsgBuf->size());
        if (newMsgBuf)
            delete newMsgBuf;
        newMsgBuf = nullptr;
    }
    msgProc.msgBatchSize++;
    _allMsgBatchSize++;

    if (_allMsgBatchSize >= _maxBatchSize) {
        if (auto res = pushTransaction())
            return res;
    }

    return 0;
}

void TermLogic::makeDecimal(ce::buffer_t *msgBuffer, int64_t valBase10, int8_t precision)
{
    msgBuffer->reserve(msgBuffer->size() + 34); //30 bytes approx max decimal
    Numeric *pDecimal = (Numeric*) (msgBuffer->end() + sizeof(int32_t));
    if (valBase10 >= 0) {
        pDecimal->signScale = NUMERIC_POS;
    } else {
        pDecimal->signScale = NUMERIC_NEG;
        valBase10 = -valBase10;
    }
    pDecimal->cntDigits = htons(toBase10000(pDecimal->digits, valBase10, precision));
    pDecimal->width = htons((uint16_t ) precision);

    auto decWid = ntohs(pDecimal->width) / NUMERIC_DIGITS
            + (ntohs(pDecimal->width) % NUMERIC_DIGITS ? 1 : 0);
    pDecimal->exp = htons(ntohs(pDecimal->cntDigits) - decWid - 1); //1 - нулевая степень

    int32_t fieldSize =
            htonl(
                    (int32_t)(ntohs(pDecimal->cntDigits) + NUMERIC_DIGITS) * sizeof(uint16_t));
    writeToBuf(msgBuffer, &fieldSize, sizeof(int32_t));
    fieldSize = ntohl(fieldSize);

    msgBuffer->resize(msgBuffer->size() + fieldSize);
}

uint16_t TermLogic::toBase10000(uint16_t *dst, int64_t valBase10, size_t precision)
{
    int64_t tmp = valBase10;
    uint16_t foursCnt;

    auto pow10 = [](uint8_t pow) {
        uint16_t res = 10;
        while(--pow)
        res *= 10;
        return res;
    };

    auto zerosCnt = precision % 4;
    for (foursCnt = 0; tmp; ++foursCnt) {
        uint16_t hi;
        if (!foursCnt && zerosCnt) {
            auto decDiv = pow10(zerosCnt);
            auto decExp = pow10(4 - zerosCnt);

            hi = (tmp % decDiv) * decExp;

            tmp /= decDiv;
        } else {
            hi = tmp % 10000;
            tmp /= 10000;
        }
        dst[foursCnt] = hi;
    }

    //swap words
    for (size_t pos = 0; pos < foursCnt / 2; ++pos) {
        uint16_t tmp = dst[pos];
        dst[pos] = htons(dst[foursCnt - pos - 1]);
        dst[foursCnt - pos - 1] = htons(tmp);
    }
    if (foursCnt % 2)
        dst[foursCnt / 2] = htons(dst[foursCnt / 2]);

    return foursCnt;
}

void TermLogic::destroy( )
{
    delete _flattener;
    if (_schema) {
        ce_schema_free(_schema);
        _schema = nullptr;
    }
}

void TermLogic::writeToBuf(ce::buffer_t *dst, const void *src, size_t size)
{
    dst->reserve(dst->size() + size);
    memcpy(dst->end(), src, size);
    dst->resize(dst->size() + size);
}

int TermLogic::processDataMsg(const ce::Link *link, const ce_msg_t *msg)
{
    if (link == _flushLink) {
        if (auto res = pushTransaction())
            return res;
    } else { //_inLink
        //Update last seq
        auto &msgProc = _msgProc[msg->msgid];
        if (msg->seq > _currentSeq - (_seqDuplicate && msgProc.msgUpsertColList.size() ? 1 : 0)) {
           _currentSeq = msg->seq;
           if (auto writeRes = writeMessage(msg, msgProc))
               return writeRes;
        } else {
            _log->trace("Message is already stored with seq = %zd (%zd)", msg->seq, _lastSeq);
        }
    }
    return 0;
}

int TermLogic::processStateMsg(const ce::Link *link, const ce_msg_t *msg)
{
    if (link == _inLink && msg->msgid == CE_MSG_STATE_ACTIVE) {
        if (!link->schema())
            return _log->error_ret(EINVAL, "link schema is NULL");
    }
    return 0;
}

int TermLogic::processSchemaOptions( )
{
    for (auto msg = _schema->messages; msg; msg = msg->next) {
        if (msg->id == 0)
            continue;
        auto &msgProc = _msgProc[msg->id];
        msgProc.table = msg->name;
        msgProc.autoTable = _autoTable;
        for (auto opt = msg->options; opt; opt = opt->next) { //Parse options
            const std::string &option = opt->option;
            const std::string &value = opt->value;
            if (option == "xpds-table" || option == "xpds-upsert-table") {
                msgProc.table = value;
            } else if (option == "pmap") {
                msgProc.msgUpsertPmap = value == "yes" || value == "true" || value == "1";
            } else if (option == "xpds-upsert-key") {
                //Split
                auto start = value.begin();
                for (auto it = value.begin(); it != value.end(); ++it) {
                    if (*it == ',') {
                        msgProc.msgUpsertColList.push_back(std::string(start, it));
                        while (*(std::next(it)) == ' ')
                            ++it;
                        start = ++it;
                    }
                }
                msgProc.msgUpsertColList.push_back(std::string(start, value.end()));
            } else if (option == "auto-table") {
                msgProc.autoTable = value == "yes" || value == "true" || value == "1";
            } else if (option == "storage.fillfactor") {
                auto fillfactor =  ce::conv::to_any<int>(value);
                if (!fillfactor)
                    return _log->error_ret(EINVAL, "fillfactor: %s, expected: 10-100", fillfactor.c_str());
                if (*fillfactor > 100 || *fillfactor < 10) {
                    return _log->error_ret(EINVAL,
                            "Incorrect value of fillfactor, val: %s, expected: 10-100",
                            value.c_str());
                }
                msgProc.fillfactor = *fillfactor;
            }
        } //Parse options end
        //Check for empty fillfactor option
        if (msgProc.fillfactor == -1) {
            if (msgProc.msgUpsertColList.size())
                msgProc.fillfactor = 10;
            else
                msgProc.fillfactor = 100;
        }
    }
    return 0;
}

int TermLogic::processOldTables()
{
    bool append = false;
    for (auto msg = _schema->messages; msg; msg = msg->next) {
        if (msg->id == 0)
            continue;
        auto itMsgProc = _msgProc.find(msg->id);
        if (itMsgProc == _msgProc.end())
            return _log->error_ret(EINVAL, "No message processing info for id %d", msg->id);
        auto &msgProc = _msgProc[msg->id];
        if (msgProc.autoTable) {
            if (_dropBefore) {
                if (auto r = dropTable(msgProc.table))
                    return r;
                _flattener->setSchema(msgProc, msg);
                if (!_flattener->parseSchema(msgProc, _unlogged))
                    return _log->error_ret(EINVAL, "Error in flattener");
            } else { // !_dropBefore
                //Check if table fields are compatible
                if (!isTableCompatible(msg))
                    return _log->error_ret(EINVAL, "Old table and schema are not compatible. name: %s", msg->name);
                append = isTableExists(msgProc.table);
            }
        } else { // !msgProc.autoTable
            //Check if table fields are compatible
            if (!isTableExists(msgProc.table))
                return _log->error_ret(EINVAL,
                        "Table %s doesn't exists or check table exists query executes with error",
                        msgProc.table.c_str());
            if (!isTableCompatible(msg))
                return _log->error_ret(EINVAL, "Old table and schema are not compatible. name: %s", msg->name);

            if (_dropBefore) {
                auto cleanQuery = ce::strprintf("TRUNCATE TABLE \"%s\"", msgProc.table.c_str());
                auto res = PQexec(_pConnDb, cleanQuery.c_str());
                auto status = PQresultStatus(res);
                PQclear(res);
                if (status != PGRES_COMMAND_OK) {
                    return _log->error_ret(1, "Can't clean non-auto table, what: %s",
                            PQerrorMessage(_pConnDb));
                }
            } else {
                append = true;
            }
        }
    }

    if (append)
        _lastSeq = _currentSeq = getLastSeq();
    return 0;
}

int TermLogic::processNewTables()
{
    for (auto &kv : _msgProc) {
        auto &msgProc = kv.second;
        auto query = msgProc.creationQuery;
        //Create Enum types
        const auto &enumQueries = msgProc.enumQueries;
        for (const auto &enumQuery : enumQueries) { //Create enum types
            _log->debug("%s", enumQuery.c_str());
            auto res = PQexec(_pConnDb, enumQuery.c_str());
            auto status = PQresultStatus(res);
            PQclear(res);
            if (status != PGRES_COMMAND_OK) {
                return _log->error_ret(EINVAL, "Can't create enum type, query: %s, what: %s",
                        enumQuery.c_str(), PQerrorMessage(_pConnDb));
            }
        }

        _log->debug("%s", query.c_str());
        auto res = PQexec(_pConnDb, query.c_str());
        auto status = PQresultStatus(res);
        PQclear(res);
        if (status != PGRES_COMMAND_OK) {
            return _log->error_ret(1, "cannot create table: %s, query: %s",
                    PQerrorMessage(_pConnDb), query.c_str());
        }

        //Write header of batch
        auto &msgBatchBuf = msgProc.msgBuffers;
        writeToBuf(&msgBatchBuf, HEADER_BYTES, HEADER_SIZE);

        //Create temp table and prepared statemets
        std::string copyStatement;
        if (msgProc.msgUpsertColList.size()) {
            query = ce::strprintf("CREATE temp TABLE \"%s_temp\" WITH (fillfactor = %d) AS SELECT * FROM \"%s\" WHERE 1 = 0;",
                    msgProc.table.c_str(), msgProc.fillfactor, msgProc.table.c_str());
            res = PQexec(_pConnDb, query.c_str());
            status = PQresultStatus(res);
            PQclear(res);
            if (status != PGRES_COMMAND_OK) {
                return _log->error_ret(EINVAL, "Can't create temp table %s, query: %s, what: %s",
                        msgProc.table.c_str(), query.c_str(), PQerrorMessage(_pConnDb));
            }

            //Create copy statement
            copyStatement = ce::strprintf("COPY \"%s_temp\" FROM STDIN (FORMAT binary);", msgProc.table.c_str());

            //Create upsert statement
            auto prep = ce::strprintf("INSERT INTO \"%s\" SELECT * FROM \"%s_temp\" ON CONFLICT (",
                    msgProc.table.c_str(), msgProc.table.c_str());
            for (const auto &keyCol : msgProc.msgUpsertColList)
                prep += keyCol + ",";
            prep.back() = ')';
            prep += " DO UPDATE SET ";
            for (const auto &col : msgProc.msgColumns) {
                bool isKey = false;
                for (const auto &keyCol : msgProc.msgUpsertColList) {
                    if (col.pField->name == keyCol) {
                        isKey = true;
                        break;
                    }
                }
                if (!isKey) { //обновляем поле если оно не ключевое
                    if (msgProc.msgUpsertPmap) {
                        prep += ce::strprintf("\"%s\" = COALESCE(EXCLUDED.\"%s\", \"%s\".\"%s\"),\n",
                                col.pField->name, col.pField->name, msgProc.table.c_str(),
                                col.pField->name);
                    } else {
                        prep += ce::strprintf("\"%s\" = EXCLUDED.\"%s\",\n", col.pField->name,
                                col.pField->name);
                    }
                }
            }
            prep += "\"seq\" = EXCLUDED.\"seq\";";

            res = PQprepare(_pConnDb,
                    ce::strprintf("%s_prep_upsert", msgProc.table.c_str()).c_str(), prep.c_str(), 0,
                    nullptr);
            status = PQresultStatus(res);
            PQclear(res);
            if (status != PGRES_COMMAND_OK) {
                return _log->error_ret(EINVAL, "Can't create prepared statement %s, what: %s",
                        prep.c_str(), PQerrorMessage(_pConnDb));
            }
            _log->trace("STATEMENT PREPARED: %s_prep_upsert", msgProc.table.c_str());
        } else {
            copyStatement = ce::strprintf("COPY \"%s\" FROM STDIN (FORMAT binary);", msgProc.table.c_str());
        }
        res = PQprepare(_pConnDb, ce::strprintf("%s_prep_copy", msgProc.table.c_str()).c_str(),
                copyStatement.c_str(), 0, nullptr);
        if (status != PGRES_COMMAND_OK) {
            return _log->error_ret(EINVAL, "Can't create prepared statement %s, what: %s",
                    copyStatement.c_str(), PQerrorMessage(_pConnDb));
        }
    }
    return 0;
}

bool TermLogic::isTableCompatible(ce_schema_msg_t *msg)
{
    auto &msgProc = _msgProc[msg->id];
    const auto &tableName = msgProc.table;
    auto oldColDescrQuery = ce::strprintf("SELECT column_name,udt_name,is_nullable,ordinal_position "
            "FROM information_schema.columns "
            "WHERE table_name = '%s' and table_schema = current_schema() ORDER BY ordinal_position;", tableName.c_str());
    auto res = PQexec(_pConnDb, oldColDescrQuery.c_str());
    bool equals = true;
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        return _log->error_ret(false, "Can't get old table description; what: %s",
                PQerrorMessage(_pConnDb));
    } else {
        using OldColMeta = struct {
            std::string name;
            std::string type;
            bool nullable;
        };
        using OldColMetaList = std::list<OldColMeta>;
        OldColMetaList oldTableMeta;

        auto rowCnt = PQntuples(res);
        for (int row = 0; row < rowCnt; ++row) {
            const std::string colName = PQgetvalue(res, row, 0);
            const std::string colType = PQgetvalue(res, row, 1);
            const bool colNullable = std::string(PQgetvalue(res, row, 2)) == "YES";
            oldTableMeta.emplace_back(OldColMeta{colName, colType, colNullable});
        }
        _flattener->setSchema(msgProc, msg);
        if (!_flattener->parseSchema(msgProc, _unlogged))
            return _log->error_ret(false, "Error in flattener");
        const auto &newColumns = msgProc.msgColumns;
        if (oldTableMeta.size() && newColumns.size() != oldTableMeta.size() - 1) { //except seq
            _log->error("Columns numbers not the same %zd != %zd", newColumns.size(), oldTableMeta.size() - 1);
            equals = false;
        } else { //False if table doesn't exist
            auto itOld = oldTableMeta.begin();
            auto itNew = newColumns.begin();
            while (itNew != newColumns.end() && itOld != oldTableMeta.end()) {
                bool newNullable = true;
                for (const auto &key : msgProc.msgUpsertColList) {
                    if (key == itOld->name) {
                        newNullable = false;
                        break;
                    }
                }
                if (itOld->name != itNew->pField->name) {
                    _log->error("Names is not equal: %s != %s", itOld->name.c_str(), itNew->pField->name);
                    equals = false;
                    break;
                }
                if(!typesCompare(msgProc, itOld->type, *itNew)) {
                    _log->error("Types no compare for field %s (%s)", itNew->pField->name, itOld->type.c_str());
                    equals = false;
                    break;
                }
                if(newNullable != itOld->nullable) {
                    _log->error("Nullable not equals");
                    equals = false;
                    break;
                }
                itOld++, itNew++;
            }
        }
    }
    PQclear(res);
    return equals;
}

bool TermLogic::isTableExists(const std::string &tableName) const
{
    auto existsQuery = ce::strprintf(
            "select exists (select 1 from pg_tables where tablename = '%s');", tableName.c_str());
    auto res = PQexec(_pConnDb, existsQuery.c_str());
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        PQclear(res);
        return false;
    }
    const std::string exists = PQgetvalue(res, 0, 0);
    PQclear(res);
    _log->trace("tablename: %s, exists: %s", tableName.c_str(), exists.c_str());
    return exists == "t";
}

int TermLogic::dropTable(const std::string &tableName) const
{
    auto getEnumsQuery = ce::strprintf(
        "select udt_name from information_schema.columns where table_name = '%s' AND data_type = 'USER-DEFINED';",
        tableName.c_str());
    auto enumsRes = PQexec(_pConnDb, getEnumsQuery.c_str());
    if (PQresultStatus(enumsRes) != PGRES_TUPLES_OK)
        return _log->error_ret(EINVAL, "Can't obtain list of enum types for table %s", tableName.c_str());

    auto dropQuery = ce::strprintf("drop table if exists \"%s\";", tableName.c_str());
    auto res = PQexec(_pConnDb, dropQuery.c_str());
    auto status = PQresultStatus(res);
    PQclear(res);
    if (status != PGRES_COMMAND_OK) {
        return _log->error_ret(EINVAL, "Can't drop table %s, query: %s, what: %s",
                tableName.c_str(), dropQuery.c_str(), PQerrorMessage(_pConnDb));
    }

    auto rowsCnt = PQntuples(enumsRes);
    for (int row = 0; row < rowsCnt; ++row) {
        std::string enumName = PQgetvalue(enumsRes, row, 0);
        std::string dropEnumQuery = ce::strprintf("drop type if exists %s cascade;", enumName.c_str());
        auto res = PQexec(_pConnDb, dropEnumQuery.c_str());
        auto status = PQresultStatus(res);
        PQclear(res);
        if (status != PGRES_COMMAND_OK)
            return _log->error_ret(EINVAL, "Can't drop enum type %s", enumName.c_str());
    }
    return 0;
}

int64_t TermLogic::getLastSeq( ) const
{
    std::string query = "select max(seqMax) from (";
    for (auto it = _msgProc.begin(); it != _msgProc.end();) {
        auto val = it->second;
        query += ce::strprintf("select max(seq) as seqMax from \"%s\"", val.table.c_str());
        if (++it != _msgProc.end())
            query += "\nunion all\n";
        query += ") as seqMax;";
    }
    auto res = PQexec(_pConnDb, query.c_str());
    int64_t seq = -1;
    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
        std::stringstream sstream(std::string(PQgetvalue(res, 0, 0)));
        sstream >> seq;
    } else {
        _log->warn("Can't read seq value from db, query: %s, message: %s", query.c_str(),
                PQerrorMessage(_pConnDb));
    }
    PQclear(res);
    return seq;
}

int TermLogic::processSysMsg(const ce::Link *link, const ce_msg_t *msg)
{
    if (msg->msgid == CE_MSG_SYSTEM_IDLE) {
        auto res = pushTransaction();
        if (res)
            return res;
    }
    return 0;
}

bool TermLogic::typesCompare(const MsgProc &msgProc, const std::string &dbType,
        const flat::ColumnInfo &ceField) const
{
    switch (ceField.pField->type) {
    case ce_schema_field_t::Int64:
        return dbType == "int8" || dbType == "timestamp";
    case ce_schema_field_t::Int32:
        return dbType == "int4";
    case ce_schema_field_t::Int16:
        //falltrough
    case ce_schema_field_t::Int8:
        return dbType == "int2";
    case ce_schema_field_t::Decimal:
        return dbType == "numeric";
    case ce_schema_field_t::Decimal128:
        return false;
    case ce_schema_field_t::Double:
        return dbType == "float8";
    case ce_schema_field_t::String:
        return dbType == "varchar";
    case ce_schema_field_t::VString:
        return dbType == "text";
    case ce_schema_field_t::Bytes:
        return dbType == "bytea";
    case ce_schema_field_t::Enum:
        if (ceField.enumAsString) {
            std::string expected = msgProc.table + "_" + ceField.pField->enum_desc->name;
            if (dbType != expected)
                return _log->error_ret(false, "Expected type %s, but %s", dbType.c_str(), expected.c_str());
            else
                return true;
        }
        else
            return dbType == "int4";
    case ce_schema_field_t::Message:
        return ceField.pField->type_msg->name == std::string("t_number")
               && msgProc.tNumAsNumeric
               && dbType == "numeric";
    default:
        return false;
    }
}

int TermLogic::pushTransaction( )
{
    auto res = PQexec(_pConnDb, "BEGIN");
    auto status = PQresultStatus(res);
    PQclear(res);
    if (status != PGRES_COMMAND_OK)
        return _log->error_ret(EINVAL, "%s", PQerrorMessage(_pConnDb));

    for (auto &msg : _msgProc) {
        if (auto pushRes = pushDataToDb(msg.second))
            return pushRes;
    }
    res = PQexec(_pConnDb, "COMMIT");
    status = PQresultStatus(res);
    PQclear(res);
    if (status != PGRES_COMMAND_OK)
        return _log->error_ret(EINVAL, "%s", PQerrorMessage(_pConnDb));
    _lastSeq = _currentSeq; //Если OK обновляем lastSeq
    _allMsgBatchSize = 0;
    return 0;
}

char* TermLogic::configCallback(const ce_config_t *cfg, void *data)
{
    auto logic = static_cast<TermLogic *>(data);
    if (logic->_lastSeq == -1)
        return strdup(logic->_confString.c_str());
    return strdup( ce::strprintf("seq=%ld", logic->_currentSeq + 1).c_str() );
}

ce::LogicImpl<TermLogic> dump_logic_impl;

int init(ce_container_t *c)
{
    ce_container_register(c, "term_cpp", &dump_logic_impl);
    return 0;
}

ce_container_module_t module = { init, 0 };

