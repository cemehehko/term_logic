/*
 * Flattener.h
 *
 *  Created on: 2 окт. 2019 г.
 *      Author: agapovmy
 */

#ifndef FLATTENER_H_
#define FLATTENER_H_

#include "msgproctypes.h"

#include <stdexcept>

// common environment
#include <ce/schema.h>
#include <ce/link.h>
#include <ce/schema/format.h>

//template types tricks
#include <type_traits>

//containers
#include <string>
#include <list>
#include <vector>
#include <unordered_map>


namespace flat {
class Flattener;
}

using namespace msgproc;

class flat::Flattener
{
public:
    Flattener(const std::string &logPrefix = "", ce_schema_msg_t *schema = nullptr);

    bool parseSchema(MsgProc &msgProc, const bool unlogged);

    virtual ~Flattener( );

    void setSchema(MsgProc &msgProc, ce_schema_msg_t *schema);

    ce_schema_msg_t* schema( ) const;

private:
    ce::Logger *_log;
    ce_schema_msg_t *_schema = nullptr;
    bool _needUpdate = true;

    bool parseSchemaRec(MsgProc &msgProc, const std::string &prefix, std::string & query, ce_schema_field_t *fields,
            const uint32_t &repNum = 0, const size_t &msgOffset = 0,
            const bool &pmap = false);

    template<typename T>
    T extractFromData(const void *data, const size_t &offset) const;

    template<typename T>
    typename std::enable_if<std::is_same<T, std::string>::value, std::string>::type
    extractFromData(const void *data, const size_t &offset, const size_t &len) const;

    template<typename T>
    typename std::enable_if<std::is_same<T, Bytes>::value, Bytes>::type
    extractFromData(const void *data, const size_t &offset, const size_t &len) const;

    std::string toBytea(const Bytes &bytes) const;

    ce_schema_enum_value_t* findInEnum(const ce_schema_enum_t *where, const int64_t &val) const;

    void createEnumType(MsgProc &msgProc, const ce_schema_field_t *enumCol);

};

#endif /* FLATTENER_H_ */
