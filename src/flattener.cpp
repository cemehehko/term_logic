/*
 * Flattener.cpp
 *
 *  Created on: 2 окт. 2019 г.
 *      Author: agapovmy
 */

#include "flattener.h"

namespace flat {

Flattener::Flattener(const std::string &logPrefix, ce_schema_msg_t *schema) :
        _log(new ce::Logger(logPrefix + ".flattener")),
        _schema(schema)
{
}

bool Flattener::parseSchema(MsgProc &msgProc, const bool unlogged)
{
    const auto &tableName = msgProc.table;
    const auto pmap = msgProc.msgUpsertPmap;
    const auto &upsertColList = msgProc.msgUpsertColList;

    auto &query = msgProc.creationQuery;
    if (!_schema)
        return false;
    if (!msgProc.msgColumns.empty()) {
        if (_needUpdate) {
            msgProc.msgColumns.clear();
            _needUpdate = false;
        } else {
            return true;
        }
    }

    query = ce::strprintf("CREATE %sTABLE IF NOT EXISTS \"%s\" (",
            unlogged ? "UNLOGGED " : "", tableName.c_str());

    auto parseResult = parseSchemaRec(msgProc, "", query, _schema->fields, 0, 0, pmap);

    if (!parseResult)
        return false;

    query += "\"seq\" int8";

    if (upsertColList.size()) {
        query += ", PRIMARY KEY(";
        for (const auto &keyCol : upsertColList) {
            query += keyCol + ",";
        }
        query.back() = ')';
    }
    query += ce::strprintf(") WITH (fillfactor = %d);", msgProc.fillfactor);


    return parseResult;
}

Flattener::~Flattener( )
{
    delete _log;
}

void Flattener::setSchema(MsgProc &msgProc, ce_schema_msg_t *schema)
{
    if (_schema != schema) {
        _schema = schema;
        msgProc.enumQueries.clear();
        _needUpdate = true;
    } else {
        _needUpdate = false;
    }
}

ce_schema_msg_t* Flattener::schema( ) const
{
    return _schema;
}


bool Flattener::parseSchemaRec(MsgProc &msgProc, const std::string &prefix, std::string &query,
        ce_schema_field_t *fields, const uint32_t &repNum,
        const size_t &msgOffset, const bool &pmap)
{
    for (auto fieldsDescr = fields; fieldsDescr; fieldsDescr = fieldsDescr->next){
        bool enumAsString = true;
        if (fieldsDescr->type == ce_schema_field_t::Message) {
            auto asNumericStr = ce::schema::__get_option(fieldsDescr->options, "xpds-numeric");
            msgProc.tNumAsNumeric =
                    asNumericStr ?
                            *asNumericStr == "true" || *asNumericStr == "yes"
                            || *asNumericStr == "1" :
                                false;
            if (msgProc.tNumAsNumeric && fieldsDescr->type_msg->name == std::string("t_number")) {
                if (fieldsDescr->type_msg->field_count != 2)
                    return _log->error_ret(false, "Incorrect t_number format: field count != 2");
                if (fieldsDescr->type_msg->fields->type != ce_schema_field_t::Int64
                    || fieldsDescr->type_msg->fields->next->type != ce_schema_field_t::Int8) {
                    return _log->error_ret(false,
                            "Incorrect t_number format: types are: (%d,%d), expected: %d,%d (Int64,Int8)",
                            fieldsDescr->type_msg->fields->type,
                            fieldsDescr->type_msg->fields->next->type,
                            ce_schema_field_t::Int64,
                            ce_schema_field_t::Int8);
                }

                query += "\"" + prefix + fieldsDescr->name + "\" numeric,\n";
                msgProc.msgColumns.push_back( { fieldsDescr, prefix, "" });
            } else if (fieldsDescr->max_count > 1) {
                for (uint32_t _it = 0; _it < fieldsDescr->max_count; ++_it) {
                    if (!parseSchemaRec(msgProc, prefix + fieldsDescr->name + "_", query,
                            fieldsDescr->type_msg->fields,  _it + 1, msgOffset +
                            fieldsDescr->offset + (_it * fieldsDescr->type_size), pmap)){
                        return false;
                    }
                }
            } else {
                if (!parseSchemaRec(msgProc, prefix + fieldsDescr->name + "_", query,
                        fieldsDescr->type_msg->fields, 0, msgOffset + fieldsDescr->offset, pmap)) {
                    return false;
                }
            }
        } else {
            std::string type, resolution;;
            switch (fieldsDescr->type) {
            case ce_schema_field_t::Int8:
                //Same with Int16
            case ce_schema_field_t::Int16:
                type = "SMALLINT";
                break;
            case ce_schema_field_t::Int32:
                type = "INT";
                break;
            case ce_schema_field_t::Int64:
                for (auto opt = fieldsDescr->options; opt; opt = opt->next) {
                    if (opt->option == std::string("time"))
                        type = "timestamp";
                    else if (opt->option == std::string("resolution"))
                        resolution = opt->value;
                }
                if (type.empty())
                    type = "BIGINT";
                break;
            case ce_schema_field_t::Double:
                type = "double precision";
                break;
            case ce_schema_field_t::String:
                type = "VARCHAR(" + std::to_string(fieldsDescr->size) + ")";
                break;
            case ce_schema_field_t::Bytes:
                type = "BYTEA";
                break;
            case ce_schema_field_t::Message:
                //Nothing to do
                break;
            case ce_schema_field_t::Decimal:
                type = "NUMERIC";
                break;
            case ce_schema_field_t::Enum: {
                auto enumAsStringStr = ce::schema::__get_option(fieldsDescr->options, "xpds-string-enum");
                enumAsString =
                        enumAsStringStr ?
                                *enumAsStringStr == "true" || *enumAsStringStr == "yes"
                                || *enumAsStringStr == "1" :
                                true;
                if (!enumAsString) {
                    type = "INT";
                } else {
                    type = msgProc.table + "_" + fieldsDescr->enum_desc->name;
                    createEnumType(msgProc, fieldsDescr);
                }
                break;
            }
            case ce_schema_field_t::VString:
                type = "TEXT";
                break;
            case ce_schema_field_t::Decimal128:
                type = "NUMERIC";
                break;
            default:
                break;
            }

            if (!(fieldsDescr->name == std::string("_pmap") && pmap)) {
                query += "\"" + prefix + fieldsDescr->name + "\" " + type + ",\n";
                fieldsDescr->offset += msgOffset;
                msgProc.msgColumns.push_back({fieldsDescr, prefix, resolution, enumAsString});
                enumAsString = true;
            }
        }
    }
    return true;
}

template<typename T>
T Flattener::extractFromData(const void *data, const size_t &offset) const
{
    return *(T*) ((uint8_t*) data + offset);
}

template<typename T>
typename std::enable_if<std::is_same<T, std::string>::value, std::string>::type Flattener::extractFromData(
        const void *data, const size_t &offset, const size_t &len) const
{
    auto ptr = (char*) data + offset;
    return std::string(ptr, ptr + len);
}

template<typename T>
typename std::enable_if<std::is_same<T, Bytes>::value, Bytes>::type Flattener::extractFromData(
        const void *data, const size_t &offset, const size_t &len) const
{
    auto ptr = (int8_t*) data + offset;
    return Bytes(ptr, ptr + len);
}

std::string Flattener::toBytea(const Bytes &bytes) const
{
    char octal[5] = { 0 };
    std::string bytea_str;
    for (const auto &byte : bytes) {
        sprintf(octal, "\\%.3o", byte);
        bytea_str += byte;
    }
    return bytea_str;
}

ce_schema_enum_value_t* Flattener::findInEnum(const ce_schema_enum_t *where,
        const int64_t &val) const
{
    auto enum_val_lst = where->values;
    while (enum_val_lst) {
        if (val == enum_val_lst->value) {
            break;
        }
    }
    return enum_val_lst;
}

void Flattener::createEnumType(MsgProc &msgProc, const ce_schema_field_t *enumCol)
{
    auto query = ce::strprintf("DO $$\nBEGIN\nIF NOT EXISTS (SELECT 1 FROM pg_type"
            " WHERE typname = '%s_%s') THEN\nCREATE TYPE \"%s_%s\" AS ENUM (",
            msgProc.table.c_str(), enumCol->enum_desc->name, msgProc.table.c_str(),
            enumCol->enum_desc->name);

    _log->trace("Enum query: %s", query.c_str());

    auto enumValues = enumCol->enum_desc->values;

    while (enumValues) {
        query += std::string("'") + enumValues->name + "'";
        if (enumValues->next)
            query += ", ";
        enumValues = enumValues->next;
    }

    query += ");"
         " END IF;"
         " END $$;";

    msgProc.enumQueries.push_back(query);
}

} /* END OF namespace flat */

