#ifndef MSGPROCTYPES_H_
#define MSGPROCTYPES_H_

#include <string>
#include <unordered_map>
#include <tuple>
#include <list>

#include <ce/util/buffer.h>
#include <ce/schema.h>

namespace flat {

struct ColumnInfo
{
    ce_schema_field_t * pField;
    std::string         prefix;
    std::string         resolution;
    //For enum
    bool                enumAsString = true;
    std::unordered_map<std::string, size_t> dbDataOffset; //Для воостановления данных pmap
};

typedef std::list<flat::ColumnInfo> ColumnList;
typedef std::list<std::string> StringList;
typedef std::vector<int8_t> Bytes;
}

namespace msgproc {

typedef std::unordered_map<std::string, ce::buffer_t> UniqMessages;
typedef int msgid_t;
typedef std::tuple<std::string, std::string, bool> oldColMeta;
typedef std::list<oldColMeta> oldColMetaList;

struct MsgProc
{
    //Flattener outputs
    std::string creationQuery;
    flat::StringList enumQueries;
    bool tNumAsNumeric;

    uint32_t msgBatchSize;
    ce::buffer_t msgBuffers;
    flat::ColumnList msgColumns;
    std::string table;

    //Options
    std::list<std::string> msgUpsertColList;
    bool msgUpsertPmap;
    bool autoTable = true;
    int fillfactor = -1;
    //Upsert mode
    UniqMessages uniqMsg;
};
}

#endif //MSGPROCTYPES_H_
