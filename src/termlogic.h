/*
 * clogic.h
 *
 *  Created on: 1 окт. 2019 г.
 *      Author: agapovmy
 */

#ifndef CLOGIC_H_
#define CLOGIC_H_

#include "flattener.h"
#include "msgproctypes.h"
#include <fstream>

//libpqpq
#include <libpq-fe.h>

#include <ce/schema/format.h>
#include <ce/logger.h>
#include <ce/container.h>
#include <ce/util/buffer.h>
#include <ce/link/base.h>
#include <ce/util/decimal128.h>
#include <ce/link/system.h>

#include <sstream>
#include <string>
#include <list>
#include <map>
#include <tuple>
#include <stdint.h>

#include <netinet/in.h>

#define htonll(x) ((((uint64_t)htonl(x)) << 32) + htonl((x) >> 32))

const uint16_t NUMERIC_SIGN_MASK =  0xC000;
const uint16_t NUMERIC_POS =        0x0000;
const uint16_t NUMERIC_NEG =        0x0040;
const uint16_t NUMERIC_SHORT =      0x8000;
const uint16_t NUMERIC_NAN   =      0xC000;
#define NUMERIC_DIGITS      4   //Count of decimal digits in Base10000

const uint64_t U_SEC_1970_2000 =    946684800000000ull;


/*
 * Postgres COPY data format:
 * HEADER:19b
 * DATA:
 *   [ COL_CNT:2b | COL_SIZE:4b | DATA:COL_SIZEb
 *                ... (COL_CNT times)
 *                | COL_SIZE:4b | DATA:COL_SIZEb ]
 *
 *   ... (Batch size times)
 *
 *   [ COL_CNT:2b | COL_SIZE:4b | DATA:COL_SIZEb
 *                ... (COL_CNT times)
 *                | COL_SIZE:4b | DATA:COL_SIZEb ]
 * END_OF_BATCH_BYTES:2b
 */
const char HEADER_BYTES[]       =   "PGCOPY\n\377\r\n\0\0\0\0\0\0\0\0\0";
const auto HEADER_SIZE          =   sizeof(HEADER_BYTES) - 1;
const char END_OF_BATCH_BYTES[] =   "\377\377";
const auto END_OF_BATCH_SIZE    =   sizeof(END_OF_BATCH_BYTES) - 1;
const auto COL_CNT_SIZE         =   2ul;
const auto COL_SIZE             =   4ul;

//Postgres NULL value size
#define PG_NULL             -1

#pragma pack(push, 1)
struct Numeric {
    uint16_t cntDigits;
    uint16_t exp;
    uint16_t signScale;
    uint16_t width;
    uint16_t digits[];
};
#pragma pack(pop)

using namespace msgproc;

class TermLogic: public ce::ILogic
{
public:
    TermLogic( );

    int init(const std::string&, const ce_container_ctx_t&) override;

    int open(const std::string&) override;

    void close( ) override;

    void destroy() override;

    int process(const ce::Link*, const ce_msg_t*) override;

private:
    ce::Link                            *_inLink        = nullptr;
    ce::Link                            *_flushLink     = nullptr;
    flat::Flattener                     *_flattener     = nullptr;
    pg_conn                             *_pConnDb       = nullptr;
    std::string                         _confString;
    int64_t                             _lastSeq        = -1;
    int64_t                             _currentSeq     = -1;
    std::string                         _connConfig;
    uint32_t                            _maxBatchSize   = 0;
    std::map<msgid_t, MsgProc>          _msgProc;
    uint32_t                            _allMsgBatchSize = 0;
    bool                                _dropBefore     = false;
    bool                                _autoTable      = false;
    bool                                _seqDuplicate   = false;
    bool                                _unlogged       = true;
    ce_schema_t                         *_schema        = nullptr;

    int parseSettings(const std::string &params);

    int processDataMsg(const ce::Link *link, const ce_msg_t *msg);

    int processStateMsg(const ce::Link *link, const ce_msg_t *msg);

    int processSchemaOptions( );

    int processOldTables( );

    int processNewTables( );

    bool isTableCompatible(ce_schema_msg_t *msg);

    bool isTableExists(const std::string &tableName) const;

    int dropTable(const std::string &tableName) const;

    int64_t getLastSeq() const;

    int processSysMsg(const ce::Link *link, const ce_msg_t *msg);

    int pushDataToDb(MsgProc &msgProc);

    bool typesCompare(const MsgProc &msgProc, const std::string &dbType, const flat::ColumnInfo &ceField) const;

    int writeMessage(const ce_msg_t *msg, MsgProc &msgProc);

    void makeDecimal(ce::buffer_t *msgBuffer, int64_t valBase10, int8_t precision);

    uint16_t toBase10000(uint16_t *dst, int64_t val, size_t precision);

    void writeToBuf(ce::buffer_t *dst, const void *src, size_t size);

    int pushTransaction();

    static char *configCallback(const ce_config_t *cfg, void *data);
};

#endif /* CLOGIC_H_ */
