#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: sts=4 sw=4 et

from ce import logger
import ce.link as L
import logging
from ce.container import run
from requests.utils import to_key_val_list
logger.init()
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(levelname)-7s %(name)s: %(message)s")

#NOSE
from nose.tools import *
from nose.plugins.attrib import attr
from nose.tools import timed

#Standart
import sys
import socket
import time
import datetime
import itertools
import errno
from decimal import Decimal
import getpass

#Standart, for running container inside python script
import threading
from multiprocessing import RLock

#CE
import ce.logger, ce.container
import ce.config
from ce.logic import Logic

#Postgresql
import psycopg2

class Cont:
    conf = None
    thread = None
    cont = None
    def cont_run(self):
        self.exception = None
        try:
            self.cont.run()
        except Exception:
            self.exception = sys.exc_info()
            
    def __init__(self, url, params={}):
        self.conf = ce.config.Config(new = True)
        self.conf.load(url)
        self.conf.load_includes("container.include")
        for name, value in to_key_val_list(params):
            self.conf.set(name, str(value))
        ce.logger.configure(self.conf)
        self.cont = ce.container.Container(self.conf, new=True)
        self.cont.init()
        self.thread = threading.Thread(target = self.cont_run)
        self.thread.start()
        self.wait_start()
    
    def __del__(self):
        self.cont and self.cont.exit(0)
        self.thread and self.thread.join()
        
    def wait_start(self, target = None):
        for _ in range(50):
            level = self.conf.get('sys.container.current_level', '')
            if target and int(level) >= target:
                break
            elif level != '-1':
                break
            time.sleep(0.1)
        return True
    
    def wait_finish(self):
        self.thread.join()
        if self.exception:
            exc = Exception('Exception in controller')
            raise exc.__class__

