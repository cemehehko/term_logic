#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: sts=4 sw=4 et

from .common import *
import os
from nose.tools.nontrivial import with_setup

db_port= 0

def get_port():
    global db_port
    if (db_port == 0):
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("",0))
        s.listen(1)
        db_port = s.getsockname()[1]
        s.close()
    return db_port

def get_pg_path():
    pg_dirs = os.listdir('/usr/lib/postgresql/')
    pg_dirs = [float(el) for el in pg_dirs]
    pg_dirs.sort()
    last_ver = pg_dirs[-1]
    pg_conf_path = ''
    if (last_ver == 12.0):
        pg_conf_path = '/usr/lib/postgresql/12/bin/'
    elif (last_ver == 11.0):
        pg_conf_path = '/usr/lib/postgresql/11/bin/'
    elif (last_ver == 10.0):
        pg_conf_path = '/usr/lib/postgresql/10/bin/'
    elif (int(last_ver) == 9 or int(last_ver) == 8):
        pg_conf_path = '/usr/lib/postgresql/' + str(last_ver) + '/bin/'
    return pg_conf_path

def setup_test():
    pg_conf_path = get_pg_path()

    os.mkdir('./test_db')
    init_pg_cmd = pg_conf_path + 'initdb -D ./test_db'
    start_pg_cmd = pg_conf_path + 'pg_ctl -D ./test_db -o "-p '+ str(get_port()) +' -k ' + os.getcwd() + '/test_db" -l ./test_db/logfile start'
    os.system(init_pg_cmd)
    os.system(start_pg_cmd)

def teardown_test():
    pg_conf_path = get_pg_path()
    stop_pg_cmd = pg_conf_path + 'pg_ctl -D ./test_db stop'
    os.system(stop_pg_cmd)
    os.system('rm -d -R ./test_db')

def runContainer(schema, config, addProps={}):
    props = { 'dbusername': getpass.getuser(), 'port':str(db_port),
             'schema':schema, 'config':config }
    props.update(addProps)
    master = Cont( 'yaml://tests/yaml/test_container.yaml', props)
    master.wait_start(3)
    master.wait_finish()
    del master

def getDbData(table, enum='', dropTable=True, dropType=True):
    conn = psycopg2.connect(dbname='postgres', user=getpass.getuser(),
                            host='localhost', port=db_port)
    cursor = conn.cursor()
    
    cursor.execute('select * from ' + table + ';')
    rows = []
    for row in cursor:
        rows.append(row)
    if (dropTable):
        cursor.execute('drop table if exists ' + table + ';')
    if (dropType and len(enum) > 0):
        cursor.execute('drop type ' + enum + ';')
    conn.commit()
    conn.close()
    return rows
 
@with_setup(setup_test, teardown_test)
def test_simple_write():
    runContainer('test_simple_schema.yaml', 'test_simple_config.yaml')
    rows = getDbData('test', 'test_f8_t')
    assert_equals(len(rows), 1)
                
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[0][3], 100) #8
    assert_equals(rows[0][4], datetime.datetime(2019, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[0][5], 'abcdef') #v, {'drop', 'yes'}string
    assert_equals(rows[0][6], Decimal('10.12300000')) #decimal8
    assert_equals(rows[0][7], 101.23) #double
    assert_equals(rows[0][8], 'First') #enum
    assert_equals(rows[0][9].tobytes(), b'abcdefghij') #bytea
    assert_equals(rows[0][10], 'qwertyuio') #char10
    assert_equals(rows[0][11], 0) #seq
          
@with_setup(setup_test, teardown_test)
def test_upsert_write():
    runContainer('test_upsert_schema.yaml', 'test_upsert_config.yaml')
    rows = getDbData('test_no_pmap', 'test_no_pmap_f8_t')
    assert_equals(len(rows), 2)
            
    assert_equals(rows[0][0], 321) #64
    assert_equals(rows[0][1], 3211) #32
    assert_equals(rows[0][2], 2311) #16
    assert_equals(rows[0][3], 10) #8
    assert_equals(rows[0][4], datetime.datetime(2020, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[0][5], 'abedef') #vstring
    assert_equals(rows[0][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[0][7], 111.23) #double
    assert_equals(rows[0][8], 'Second') #enum
    assert_equals(rows[0][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[0][10], 'qwertyulo') #char10
    assert_equals(rows[0][11], 3) #seq
            
    assert_equals(rows[1][0], 123) #64
    assert_equals(rows[1][1], 3211) #32
    assert_equals(rows[1][2], 2311) #16
    assert_equals(rows[1][3], 10) #8
    assert_equals(rows[1][4], datetime.datetime(2020, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[1][5], 'abedef') #vstring
    assert_equals(rows[1][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[1][7], 111.23) #double
    assert_equals(rows[1][8], 'Second') #enum
    assert_equals(rows[1][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[1][10], 'qwertyulo') #char10
    assert_equals(rows[1][11], 2) #seq
            
@with_setup(setup_test, teardown_test)
def test_pmap_write():
    runContainer('test_pmap_schema.yaml', 'test_pmap_config.yaml')
    rows = getDbData('test_pmap', 'test_pmap_f8_t')
    assert_equals(len(rows), 2)
            
    assert_equals(rows[0][0], 321) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[0][3], 100) #8
    assert_equals(rows[0][4], datetime.datetime(2019, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[0][5], 'abcdef') #vstring
    assert_equals(rows[0][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[0][7], 111.23) #double
    assert_equals(rows[0][8], 'Second') #enum
    assert_equals(rows[0][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[0][10], 'qwertyulo') #char10
    assert_equals(rows[0][11], 3) #seq
            
    assert_equals(rows[1][0], 123) #64
    assert_equals(rows[1][1], 321) #32
    assert_equals(rows[1][2], 231) #16
    assert_equals(rows[1][3], 100) #8
    assert_equals(rows[1][4], datetime.datetime(2019, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[1][5], 'abcdef') #vstring
    assert_equals(rows[1][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[1][7], 111.23) #double
    assert_equals(rows[1][8], 'Second') #enum
    assert_equals(rows[1][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[1][10], 'qwertyulo') #char10
    assert_equals(rows[1][11], 2) #seq
     
@with_setup(setup_test, teardown_test)
def test_pmap_null():
    runContainer('test_pmap_schema.yaml', 'test_pmap_null_1_config.yaml')
    rows = getDbData('test_pmap', 'test_pmap_f8_t', dropTable=False, dropType=False)
     
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[0][3], 100) #8
    assert_equals(rows[0][4], datetime.datetime(2019, 12, 16, 0, 0)) #timestamp
    assert_equals(rows[0][5], 'abcdef') #vstring
    assert_equals(rows[0][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[0][7], 111.23) #double
    assert_equals(rows[0][8], 'Second') #enum
    assert_equals(rows[0][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[0][10], 'qwertyulo') #char10
    assert_equals(rows[0][11], 0) #seq
     
    runContainer('test_pmap_schema.yaml', 'test_pmap_null_2_config.yaml')
    rows = getDbData('test_pmap', 'test_pmap_f8_t')
     
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 3210) #32
    assert_equals(rows[0][2], 2314) #16
    assert_equals(rows[0][3], 122) #8
    assert_equals(rows[0][4], datetime.datetime(2019, 12, 17, 0, 0)) #timestamp
    assert_equals(rows[0][5], 'abvdef') #vstring
    assert_equals(rows[0][6], Decimal('11.12300000')) #decimal8
    assert_equals(rows[0][7], 111.23) #double
    assert_equals(rows[0][8], 'Second') #enum
    assert_equals(rows[0][9].tobytes(), b'abcdefbhij') #bytea
    assert_equals(rows[0][10], 'qwertyulo') #char10
    assert_equals(rows[0][11], 3) #seq
 
@with_setup(setup_test, teardown_test)
def test_log_two():
    runContainer('test_log_two_message_schema.yaml', 'test_log_two_message_config.yaml')
    rows = getDbData('test_log_0')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
            
    rows = getDbData('test_log_1')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
            
@with_setup(setup_test, teardown_test)
def test_log_upsert():
    runContainer('test_log_upsert_schema.yaml', 'test_log_upsert_config.yaml')
    rows = getDbData('test_log')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
            
    rows = getDbData('test_no_pmap')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 3211) #32
    assert_equals(rows[0][2], 2311) #16
            
@with_setup(setup_test, teardown_test)
def test_upsert_two():
    runContainer('test_upsert_two_schema.yaml', 'test_upsert_two_config.yaml')
    rows = getDbData('test_upsert_0_tbl')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 3211) #32
    assert_equals(rows[0][2], 2311) #16
            
    rows = getDbData('test_upsert_1_tbl')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 1234) #64
    assert_equals(rows[0][1], 3212) #32
    assert_equals(rows[0][2], 2312) #16
            
@with_setup(setup_test, teardown_test)
def test_autoTable_dropBefore():
    runContainer('test_autoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_0_config.yaml', {'drop': 'yes'})
    rows = getDbData('test')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
            
    runContainer('test_autoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_1_config.yaml')
    rows = getDbData('test')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 1234) #64
    assert_equals(rows[0][1], 3210) #32
    assert_equals(rows[0][2], 2314) #16
            
@with_setup(setup_test, teardown_test)
def test_autoTable_noDropBefore():
    runContainer('test_autoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_0_config.yaml')
            
    runContainer('test_autoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_1_config.yaml')
    rows = getDbData('test')
    assert_equals(len(rows), 2)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[1][0], 1234) #64
    assert_equals(rows[1][1], 3210) #32
    assert_equals(rows[1][2], 2314) #16
           
@with_setup(setup_test, teardown_test)
def test_noAutoTable_dropBefore():
    conn = psycopg2.connect(dbname='postgres', user=getpass.getuser(),
                            host='localhost', port=db_port)
    cursor = conn.cursor()
    cursor.execute('create table test (f0 int8, f1 int4, f2 int2, seq int8);')
    conn.commit()
    conn.close()
           
    runContainer('test_noAutoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_0_config.yaml', {'drop': 'yes'})
    rows = getDbData('test', dropTable=False)
    assert_equals(len(rows), 1)
           
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
           
    runContainer('test_noAutoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_1_config.yaml', {'drop': 'yes'})
    rows = getDbData('test')
    assert_equals(len(rows), 1)
           
    assert_equals(rows[0][0], 1234) #64
    assert_equals(rows[0][1], 3210) #32
    assert_equals(rows[0][2], 2314) #16
           
@with_setup(setup_test, teardown_test)
def test_noAutoTable_noDropBefore():
    conn = psycopg2.connect(dbname='postgres', user=getpass.getuser(),
                            host='localhost', port=db_port)
    cursor = conn.cursor()
    cursor.execute('create table test (f0 int8, f1 int4, f2 int2, seq int8);')
    conn.commit()
    conn.close()
           
    runContainer('test_noAutoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_0_config.yaml')
    rows = getDbData('test', dropTable=False)
    assert_equals(len(rows), 1)
           
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
           
    runContainer('test_noAutoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_1_config.yaml')
    rows = getDbData('test')
    assert_equals(len(rows), 2)
           
    assert_equals(rows[1][0], 1234) #64
    assert_equals(rows[1][1], 3210) #32
    assert_equals(rows[1][2], 2314) #16
           
@with_setup(setup_test, teardown_test)
@raises(Exception)
def test_noAutoTable_noCreate():
    runContainer('test_noAutoTable_dropBefore_schema.yaml', 'test_autoTable_dropBefore_0_config.yaml')
       
@with_setup(setup_test, teardown_test)
@raises(Exception)
def test_rollback():
    runContainer('test_rollback_schema.yaml', 'test_rollback_config.yaml')
         
    rows = getDbData('test_0')
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
            
    rows = getDbData('test_1')
    assert_equals(len(rows), 1)
        
    assert_equals(rows[1][0], Decimal('123.11')) #64
    assert_equals(rows[1][1], 321) #32
    assert_equals(rows[1][2], 231) #16
     
@with_setup(setup_test, teardown_test)
def test_t_number_decimal():
    runContainer('test_t_number_schema.yaml', 'test_t_number_config.yaml')
    rows = getDbData('test')
    assert_equals(len(rows), 1)
                
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], Decimal('543.21')) #32
    assert_equals(rows[0][2], 231) #16
         
@with_setup(setup_test, teardown_test)
def test_t_number_separately():
    runContainer('test_t_number_separately_schema.yaml', 'test_t_number_config.yaml')
    rows = getDbData('test')
    print(rows)
    assert_equals(len(rows), 1)
            
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 54321) #64
    assert_equals(rows[0][2], 2) #32
    assert_equals(rows[0][3], 231) #1
       
@with_setup(setup_test, teardown_test)
@raises(Exception)
def test_t_number_incorrect():
    runContainer('test_t_number_incorrect_schema.yaml', 'test_t_number_incorrect_config.yaml')
  
@with_setup(setup_test, teardown_test)
@raises(Exception)
def test_t_number_incorrect_2():
    runContainer('test_t_number_incorrect_schema_2.yaml', 'test_t_number_config.yaml')
    
@with_setup(setup_test, teardown_test)
def test_duplicate_seq():
    runContainer('test_upsert_two_schema.yaml', 'test_duplicate_seq_config.yaml', {'seq-dupl': 'yes'})
    rows = getDbData('test_upsert_0_tbl')
    assert_equals(len(rows), 1)
           
    assert_equals(rows[0][0], 123) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[0][3], 1) #16
           
    rows = getDbData('test_upsert_1_tbl')
    assert_equals(len(rows), 1)
           
    assert_equals(rows[0][0], 1234) #64
    assert_equals(rows[0][1], 321) #32
    assert_equals(rows[0][2], 231) #16
    assert_equals(rows[0][3], 1) #16
